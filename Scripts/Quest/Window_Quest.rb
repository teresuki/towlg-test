#==============================================================================
# ** Window_Quest
#------------------------------------------------------------------------------
#  This window displays Current Active Quest.
#  The logic of what quests will be displayed is in here.
#  
#==============================================================================

class Window_Quest < Window_Selectable
  
    def initialize(x, y, width, height)
      super(x, y, width, height)
      data = []
      @column_max = 1
      self.index = -1
      @quest_menu_selected_type = ""
      refresh
    end #initialize
    
    attr_accessor :data
    attr_accessor :index
    
    attr_accessor :quest_menu_selected_type
    
    def refresh
      if self.contents != nil
        self.contents.dispose
        self.contents = nil
      end
      
      #Refresh all quest [active] and [completed] status.
      refresh_quest_status
      
      #Load data with quests with the respected selected quest type
      if @quest_menu_selected_type != ""
        load_quest_list
      end
      
      
    end #refresh
    
    #This is for updating whether the quest is active/completed
    def refresh_quest_status
      for i in 0..$quest_list.length-1
        $quest_list[i].refresh_status()
      end
    end #refresh_quest_status
    
    def load_quest_list
      @data = []
      
      #load quest into data
      for i in 0..$quest_list.length-1
        
        #Note: a quest is considered Active
        #if only it is Active and NOT Completed
        if @quest_menu_selected_type == "Active"
          if $quest_list[i].active and !$quest_list[i].completed
            @data.push($quest_list[i])
          end
          
        #Note: a quest is considered Completed
        #if only it is Active and Completed
          elsif @quest_menu_selected_type == "Completed"
          if $quest_list[i].completed
            @data.push($quest_list[i])
          end
        end
        
      end
      
      
      #Draws the loaded quests into the Widndow
      @item_max = @data.size
      if @item_max > 0
        self.contents = Bitmap.new(width - 32, row_max * 32)
        for i in 0...@item_max
          draw_quest(i)
        end
      end
      
      
    end #load_quest_list
    
    
    def draw_quest(index)
      quest = @data[index]
      x = 4
      # For y, you can change 32 to another number to change the spacing
      y = index * 32
      rect = Rect.new(x, y, self.width / @column_max - 32, 32)
      self.contents.fill_rect(rect, Color.new(0, 0, 0, 0))
      #bitmap = RPG::Cache.icon("038-Item07")
      #opacity = 255
      #self.contents.blt(x, y + 4, bitmap, Rect.new(0, 0, 24, 24), opacity)
      self.contents.draw_text(x , y, 270, 32, quest.quest_name, 0)
    end #draw_quest
    
    
  end #Window_Quest