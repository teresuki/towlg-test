#==============================================================================
# ** Window_Quest_Detail
#------------------------------------------------------------------------------
#  This window displays Selected Quest Details.
#  Line Breaks for text are done automatically
#  Multiple pages are used if details is too long
#==============================================================================
class Window_Quest_Detail < Window_Base
  
    def initialize(x, y, width, heigth)
      super(x, y, width, heigth)
      @width = width
      @height = height
      @pages # Two Dimesion Array
      @page_index
      self.contents = Bitmap.new(width - 32, height - 32)
    end #initialize
    
    attr_accessor :page_index
    attr_reader :pages
    
    #--------------------------------------------------------------------------
    # * Loads the quest's details, run when window is active
    #     index : item number
    #--------------------------------------------------------------------------
    
    def load_quest_detail(text)
      
      #Seperate the text into words for multiple lines
      @words = text.split(/\s+/)
      
      @line = ""
      @lines = []
      
      #Max number of lines per page
      @max_line = 15
      
      
      #Get the lines to display on Window
      for i in 0..@words.length-1
        #New Line character check
        if(@words[i] == "/n")
          @lines.push(@line)
          @line = ""
          next
        end #New Line Check
        
        #New Page character check
        #Because @pages is not initialized yet,
        #We need to calculate the remaining lines in current page to 
        #make all that lines empty string
        #so we can "make" another page
         if(@words[i] == "/p")
           #First, push the current line first
           @lines.push(@line)
           @line = ""
           
           #Calculate the remaining lines
           current_line = @lines.length % @max_line
           remain_line = @max_line - current_line
           for i in 1..remain_line
             @lines.push("")
           end
           next
         end #New Page Check
        
        
         #Check if line is too long
         if(@line.length + @words[i].length < 35)
          @line += @words[i]
          @line += " "
          if i == @words.length-1 
            @lines.push(@line)
          end
        else 
          @lines.push(@line)
          @line = ""
          @line += @words[i]
          @line += " "
          if i == @words.length-1
            @lines.push(@line)
          end
        end #Check line's length (character counts)
        
      end #FOR LOOP
      
      #Calculate the total numbers of pages needed
      @num_page = (@lines.length/@max_line) + 1
      
      #Initialize the pages 2D array
      @pages = Array.new(@num_page)
      
      #push the line(s) into the page(s)
      for i in 0..@num_page-1
        page = []
        for j in 0..@max_line-1
          line = @lines[i*@max_line + j]
          if(line != nil)
            page.push(line)
          end
        end
        @pages[i] = page
      end #Push lines into pages
      
      #direct index to 0 (first page)
      @page_index = 0
      
    end #load_quest_detail
    
    #--------------------------------------------------------------------------
    # * Draws the text into the window
    #     index : item number
    #--------------------------------------------------------------------------
    def set_text(page_index, align = 0)
      self.contents.clear
      page = @pages[page_index] #Array of Strings
  
       for i in 0..page.length-1
          # Redraw text
          line = page[i]
          self.contents.font.color = normal_color
          self.contents.draw_text(4, i*24, @width, 24, line, align)
          @text = line
          @align = align
          @actor = nil
       end
        
       #get the total numbers of page
       num_of_pages = @pages.length
       current_page_text = "#{page_index + 1} in #{num_of_pages}"
       
       #Draws the current_page/number of page
       self.contents.font.color = normal_color
       self.contents.draw_text(-40, 365, @width, 24, current_page_text, 2)
       @text = current_page_text
       @align = 2
       @actor = nil
        
      self.visible = true
      
    end # set_text
    
    def clear_text
      self.contents.clear
    end #clear_text
    
    
  end #Window_Quest_Detail