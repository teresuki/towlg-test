#==============================================================================
# ** Window_Quest_Menu
#------------------------------------------------------------------------------
#  This window displays Quest Menu.
#  There are two options: [Active] and [Completed]
#==============================================================================

class Window_Quest_Menu < Window_Selectable
  
    def initialize(x, y, width, height)
      super(x, y, width, height)
      @column_max = 2
      @item_max = 2
      self.index = 0
      refresh
      data = []
    end #initialize
    
    
    def refresh
      if self.contents != nil
        self.contents.dispose
        self.contents = nil
      end
      @data = []
      # Add Active Quest and Completed Quest Option
      @data.push("Active")
      @data.push("Completed")
      
      #draw quest menu data
      self.contents = Bitmap.new(width - 32, row_max * 32)
      draw_quest_menu_data("Active")
      draw_quest_menu_data("Completed")
      
    end #refresh
    
    
    #--------------------------------------------------------------------------
    # * Draw Item
    #     index : item number
    #--------------------------------------------------------------------------
    def draw_quest_menu_data(title)
      index
      case title
       when "Active"
         index = 0
       when "Completed"
         index = 1
       end
       
      x = 4 + index % 2 * (288 + 32)
      y = index / 2 * 32
      rect = Rect.new(x, y, self.width / @column_max - 32, 32)
      self.contents.fill_rect(rect, Color.new(0, 0, 0, 0))
      bitmap = RPG::Cache.icon("038-Item07")
      opacity = 255
      self.contents.blt(x, y + 4, bitmap, Rect.new(0, 0, 24, 24), opacity)
      self.contents.draw_text(x + 28, y, 212, 32, title, 0)
    end
    
  end #Window_Quest