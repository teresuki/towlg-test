#==============================================================================
# ** Scene_Quest
#------------------------------------------------------------------------------
#  This class performs quest screen processing.
#==============================================================================
class Scene_Quest
	#--------------------------------------------------------------------------
	# * Main Processing
	#--------------------------------------------------------------------------
	def main
		#Setup 3 windows
		@quest_menu_window = Window_Quest_Menu.new(0, 0, 640, 64)
		@quest_window = Window_Quest.new(0, 64, 272, 416)
		@quest_detail_window = Window_Quest_Detail.new(272, 64, 368, 416)
    
    #disable quest_window and quest_detail_window active
		@quest_window.active = false
		@quest_detail_window.active = false
    
    # Execute transition
		Graphics.transition
		# Main loop
		loop do
			# Update game screen
			Graphics.update
			# Update input information
			Input.update
			# Frame update
			update
			# Abort loop if screen is changed
			if $scene != self
				break
			end
		end
    
    # Prepare for transition
		Graphics.freeze
    # Dispose of windows
		@quest_menu_window.dispose
		@quest_window.dispose
		@quest_detail_window.dispose
  end #main
  #--------------------------------------------------------------------------
	# * Frame Update
	#--------------------------------------------------------------------------
	def update
    # Update windows
		@quest_menu_window.update
		@quest_window.update
		#@quest_detail_window.update
    
    # If quest menu window is active: call update_quest_menu
		if @quest_menu_window.active
			update_quest_menu
			return
		end
    
    #If quest window is active: call update_quest
		if @quest_window.active
			update_quest
			return
		end
    
  end #update
  #--------------------------------------------------------------------------
	# * Frame Update (when quest menu window is active)
	#--------------------------------------------------------------------------
	def update_quest_menu
    
		# Return to Menu
		if Input.trigger?(Input::B)
			# Play cancel SE
			$game_system.se_play($data_system.cancel_se)
			# Switch to menu screen
			$scene = Scene_Menu.new(0)
			return
		end
    
    # View Quests
    if Input.trigger?(Input::C)
      @quest_menu_window.active = false
			@quest_window.active = true
			@quest_window.index = 0
			@quest_detail_window.active = true
      Audio.se_play("Audio/SE/007-System07")
			#Check the quest type selected in menu
			if @quest_menu_window.index == 0
				@quest_window.quest_menu_selected_type = "Active"
				@quest_window.refresh()
			elsif @quest_menu_window.index == 1
				@quest_window.quest_menu_selected_type = "Completed"
				@quest_window.refresh()
			end
      #Loading first quest's detail
			unless @quest_window.data[@quest_window.index] == nil
				quest_detail = @quest_window.data[@quest_window.index].quest_detail
				@quest_detail_window.load_quest_detail(quest_detail)
				@quest_detail_window.set_text(@quest_detail_window.page_index)
			end
    end
    
	end #update_quest_menu
  #--------------------------------------------------------------------------
	# * Frame Update (when quest window is active)
	#--------------------------------------------------------------------------
	def update_quest
    
    # Return to select type of quest
		if Input.trigger?(Input::B)
			# Play cancel SE
			$game_system.se_play($data_system.cancel_se)
			@quest_window.active = false
			@quest_window.index = -1
			@quest_window.quest_menu_selected_type = ""
			@quest_window.refresh()
			@quest_detail_window.active = false
			@quest_detail_window.clear_text()
			@quest_menu_window.active = true
			return
		end
    
    # If C button was pressed
		if Input.trigger?(Input::C)
      
    end
    
    #Check if there is no quest
		if @quest_window.data[@quest_window.index] != nil
      
			# If DOWN/UP button was pressed, update help text
			if Input.trigger?(Input::DOWN) or Input.trigger?(Input::UP)
				#Loads the new highlighted quest's detail
        unless @quest_window.data[@quest_window.index] == nil
					quest_detail = @quest_window.data[@quest_window.index].quest_detail
					@quest_detail_window.load_quest_detail(quest_detail)
					@quest_detail_window.set_text(@quest_detail_window.page_index)
				end
			end
      
      #Page turn LEFT or RIGHT
			if Input.trigger?(Input::LEFT) or Input.trigger?(Input::RIGHT)
        max_page = @quest_detail_window.pages.length
				can_left = (@quest_detail_window.page_index-1) >= 0
				can_right = (@quest_detail_window.page_index+1) < max_page
        #Page turn LEFT
				if Input.trigger?(Input::LEFT) && can_left
					@quest_detail_window.page_index -= 1
					@quest_detail_window.set_text(@quest_detail_window.page_index)
					Audio.se_play("Audio/SE/046-Book01")
        #Page turn RIGHT
				elsif Input.trigger?(Input::RIGHT) && can_right
					@quest_detail_window.page_index += 1
					@quest_detail_window.set_text(@quest_detail_window.page_index)
					Audio.se_play("Audio/SE/046-Book01")
        #Can no longer turn LEFT or RIGHT
				else
					Audio.se_play("Audio/SE/004-System04")
				end
        
      end # Page turn LEFT or RIGHT
      
		end # Check if no quests
    
    
  end #update_quest
  
  
end #Scene_Quest
