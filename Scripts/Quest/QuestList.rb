#===============================================================================
# Instruction:
#-------------------------------------------------------------------------------
# Create a new Quest:
# -------------------
#
# $quest_list << Quest.new("Quest_Name", Active_Switch_ID, Complete_Switch_ID, "Quest Detail")
#
# Example:
# $quest_list << Quest.new("The first day at Gensokyo", 33, 49, 
# "I was saved by Mokou after Mystia tried to attack me." +
# " Mokou and Keine said I should meet the Hakurei Shrine Maiden for my safety." +
# " I wonder what kind of person Reimu is.")
#-------------------------------------------------------------------------------
# Updating quest detail
# ---------------------
#
# Prerequisite:
#
# - First of all, for every quest that you created, it will has an index (or ID)
#
# - The index will increment by 1 with each quest you created, starting from 0
# So, for example, the first 5 quests' ID will respectively be: 0, 1, 2, 3, 4
#
# - Quest's index is necessary to update quest detail
# So I highly recommend that you comment the quest's ID before you create them
#
# You can make a comment in a line with a # symbol like this guide.
# ______________________________________________________________________________
#
# Update quest's detail by making a new paragraph:
# ---------------------------------------
# $quest_list[quest_index].update(update_switch_id, "new_quest_detail")
#
# Example:
#$quest_list[0].update(30, "I met Reimu and she was willing to let me stay" +
#" at her residence, she seemed pretty happy after I donated 2000 Yen to her." +
#" Maybe I should take note of this")
# ______________________________________________________________________________
#
# Update quest's detail by making a new page:
# ----------------------------------
#
# $quest_list[quest_index].update_page(update_switch_id, "new_quest_detail")
#
# Example:
#$quest_list[0].update_page(40, "Yukari explained that I was chosen to be " +
#" the Emissary of Gensokyo, which apparently is selected every 200 years. " +
#"Her explaination seemed fishy but I have no choice but to accept my " +
#"duty to explore Gensokyo as I wish.")
#
# ==============================================================================
# Manual line breaks and new page
# -------------------------------
#
# - Of course, you do not need to use update to 
# make a new line or make a new page. You can do it manually directly
# in the [quest_detail]
#
# Note: Foward slash [/], not backward slash [\]
# [New Line special character]: /n
# [New Page special character]: /p
#
# Example:
# quest_detail = "I'm on first line! /n I'm on second line!"
# quest_detail = "I'm on first line! /n /n I'm on third line!"
# quest_detail = "I'm on first page! /p I'm on second page!"
#
# [!Warning]: Please makes sure that there is a space between
# special character and the text itself
# For examples, these will NOT work:
# quest_detail = "I'm on first line!/n I'm on second line!"
# quest_detail = "I'm on first line! /nI'm on second line!"
#
# ==============================================================================
# [Note for quest_detail]
# - Line breaks are done automatically when a line is almost full
# if the current page is full, lines are moved to next page(s).
# ______________________________________________________________________________
# [Note about the orders of quest's updates]
# - Normally, the order of Update quest detail does not matter,
# however, if two or mote updates has the same [update_switch_id]
# the one that is written first will be executed first
#
# Example:
# This will create a new page first, then a new paragraph
# $quest_list[0].update_page(10, "A New Page!")
# $quest_list[0].update(10, "A New Paragraph!")
#
# This will create a new paragraph first, then a new page
# $quest_list[0].update(10, "A New Paragraph!")
# $quest_list[0].update_page(10, "A New Page!")
#
# => I highly recommend you to order the updates in correct order
# even if updates does not have the same [update_switch_id]
# _________________________________________________________________
#
#===============================================================================

$quest_list = []
#Add after this line!


