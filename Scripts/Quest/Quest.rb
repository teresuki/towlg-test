#===============================================================================
#                         QUEST SYSTEM
#-------------------------------------------------------------------------------
# by Teresuki Kitsune
# last update: 23-Jun-2022
# This file contains the structure of a quest
# For adding quest please open "QuestList" file
# 
#
#
# Variables' meaning explaination:
# @quest_name: The name of the quest
# @active: = TRUE if @active_switch_id is ON/TRUE, else FALSE
# @completed = TRUE if @completed_switch_id is ON/TRUE, else FALSE
# @active_switch_id = The ID of the switch that makes it [active]
# @completed_switch_id = The ID of the switch that makes it [completed]
# @quest_detail = The detail about the quest
# @quest_update: A 2D Array that store smaller arrays
#   each smaller array contains:
#   [String, int, String, boolean]
#   ["UpdateType", Update_Switch_ID, "Update_Details", has_updated?]
#
#===============================================================================

class Quest
  
    #Constructor
    def initialize(quest_name, active_switch_id, completed_switch_id, 
      quest_detail = "")
      @quest_name = quest_name
      @active = false
      @completed = false
      @active_switch_id = active_switch_id
      @completed_switch_id = completed_switch_id
      @quest_detail = quest_detail
      @quest_update = Array.new
    end #initialize
    
    attr_accessor :quest_name
    attr_accessor :active
    attr_accessor :completed
    attr_accessor :quest_detail
    attr_accessor :quest_update
    
    
    def refresh_status()
      #Checks for active status
      if $game_switches[@active_switch_id] == true
        @active = true
      else
        @active = false
      end
      
      #Checks for completed status
      if $game_switches[@completed_switch_id] == true
        @completed = true
      else
        @completed = false
      end
      
      #Checks for update quest detail
      for i in 0..@quest_update.size-1
        switch_on = ($game_switches[@quest_update[i][1]] == true)
        update_type = @quest_update[i][0]
        has_updated = @quest_update[i][3]
        
        #Type is Update
        if(switch_on && update_type == "Update" && has_updated == false)
          update_detail(@quest_update[i][2])
          @quest_update[i][3] = true
        #Type is New Page
        elsif(switch_on && update_type == "NewPage" && has_updated == false)
          update_detail_page(@quest_update[i][2])
          @quest_update[i][3] = true
        end
      
      end # check update quest detail end
      
    end #refresh_status
    
  #-------------------------------------------------------------------------------
  # Type: Update
  # Explaination: To create a new Paragraph on top of current quest's detail
  #
  #-------------------------------------------------------------------------------
    
    def update_detail(new_detail)
      @quest_detail = "#{new_detail} /n /n #{@quest_detail}"
    end #update_detail
    
    def update(switch_id, new_detail)
      data = ["Update", switch_id, new_detail, false]
      @quest_update.push(data)
      #@quest_update.store(switch_id, ["Update", new_detail, false])
    end #update_quest_when
    
  #-------------------------------------------------------------------------------
  # Type: Update_Page
  # Explaination: To create a new Page on top of current quest's detail
  #
  #------------------------------------------------------------------------------- 
    
    def update_detail_page(new_detail)
      @quest_detail = "#{new_detail} /p #{@quest_detail}"
    end #update_detail_new_page
    
    def update_page(switch_id, new_detail)
      data = ["NewPage", switch_id, new_detail, false]
      @quest_update.push(data)
      #@quest_update.store(switch_id, ["NewPage", new_detail, false])
    end #update_quest_new_page
    
  end #Quest